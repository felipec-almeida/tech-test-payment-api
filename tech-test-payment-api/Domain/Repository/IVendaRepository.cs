﻿using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Domain.Repository;

public interface IVendaRepository
{
    public Task<Venda?> Get(int id);

    public Task<ItemVenda?> GetItem(int id);

    public Task<Venda> Save(Venda venda);

    public Task<Venda> UpdateStatus(Venda venda, EStatusVenda status);

    public Task<Venda> SaveItemVenda(Venda venda, ItemVenda item);

    public Task<Venda> DeleteItemVenda(Venda venda, ItemVenda item);

    public Task<string> DeleteVenda(Venda venda);
}

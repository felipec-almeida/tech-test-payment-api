﻿namespace tech_test_payment_api.Domain.Exceptions;

public class ValidationException : Exception
{
    public string Message;
    
    public ValidationException(string message)
    {
        Message = message;
    }
}
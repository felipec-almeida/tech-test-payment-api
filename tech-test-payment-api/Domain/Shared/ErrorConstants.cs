﻿using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Domain.Shared;

public class ErrorConstants
{
    public static string ERRO_ENVIAR_VENDA = "Venda não encontrada ou não enviada.";

    public static string ERRO_ENVIAR_VENDA_ITEM = "A Venda precisa conter pelo menos um item.";

    public static string ERRO_ENVIAR_ITEM = "É preciso enviar pelo menos um item.";

    public static string ERRO_ENVIAR_ITEM_JA_EXISTENTE = "Este item já existe nesta venda.";

    public static string ERRO_ENVIAR_ITEM_NAO_EXISTENTE = "Este item não existe nesta venda.";

    public static string ERRO_ENVIAR_VENDA_VENDEDOR = "Vendedor diferente do informado.";

    public static string ERRO_ENVIAR_ITEM_STATUS_VENDA =
        $"Esta venda não pode ser alterada, pois não está com o Status de {EStatusVenda.AGUARDANDO_PAGAMENTO}.";

    public static string ERRO_ALTERAR_STATUS_AGUARDANDO_PAGAMENTO =
        $"Erro ao Alterar o Status da Venda. Selecione {EStatusVenda.PAGAMENTO_EFETUADO} ou {EStatusVenda.CANCELADO}";

    public static string ERRO_ALTERAR_STATUS_PAGAMENTO_EFETUADO =
        $"Erro ao Alterar o Status da Venda. Selecione {EStatusVenda.ENVIADO_TRANSPORTADORA} ou {EStatusVenda.CANCELADO}";

    public static string ERRO_ALTERAR_STATUS_ENVIADO_TRANSPORTADORA =
        $"Erro ao Alterar o Status da Venda. Selecione {EStatusVenda.ENTREGUE} ou {EStatusVenda.CANCELADO}";
}

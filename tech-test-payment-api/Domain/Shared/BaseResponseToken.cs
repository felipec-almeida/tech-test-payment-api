﻿namespace tech_test_payment_api.Domain.Shared
{
    public class BaseResponseToken<T>
    {
        public T Value { get; set; }
        public string Token { get; set; }

        public BaseResponseToken(T value, string token)
        {
            Value = value;
            Token = token;
        }
    }
}

﻿namespace tech_test_payment_api.Domain.Shared
{
    public class BaseResponseError
    {
        public BaseResponseError(string messageError)
        {
            Message = messageError;
        }
        
        public string Message { get; }
    }
}

namespace tech_test_payment_api.Domain.Entities
{
    public class Vendedor
    {
        private int _vendedorId;
        private string _nomeCompleto;
        private string _cpf;
        private string _email;
        private string _telefone;

        public int VendedorId
        {
            get { return this._vendedorId; }
            set { this._vendedorId = value; }
        }

        public string NomeCompleto
        {
            get { return this._nomeCompleto; }
            set { this._nomeCompleto = value; }
        }

        public string CPF
        {
            get { return this._cpf; }
            set { this._cpf = value; }
        }

        public string Email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        public string Telefone
        {
            get { return this._telefone; }
            set { this._telefone = value; }
        }
    }
}

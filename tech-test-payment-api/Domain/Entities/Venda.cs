using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.Entities
{
    public class Venda
    {
        private int _vendaId;
        private DateTime _data;
        private int _vendedorId;
        private Vendedor _vendedor;
        private List<ItemVenda> _itemVenda;
        private EStatusVenda _statusVenda;

        public int VendaId
        {
            get { return this._vendaId; }
            set { this._vendaId = value; }
        }

        [DataType(DataType.Date)]
        public DateTime Data
        {
            get { return this._data; }
            set { this._data = value; }
        }

        public int VendedorId
        {
            get { return this._vendedorId; }
            set { this._vendedorId = value; }
        }

        public Vendedor Vendedor
        {
            get { return this._vendedor; }
            set { this._vendedor = value; }
        }

        public List<ItemVenda> ItemVenda
        {
            get { return this._itemVenda; }
            set { this._itemVenda = value; }
        }

        public EStatusVenda StatusVenda
        {
            get { return this._statusVenda; }
            set { this._statusVenda = value; }
        }
    }
}

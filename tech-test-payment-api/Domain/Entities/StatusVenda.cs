namespace tech_test_payment_api.Domain.Entities
{
    public enum EStatusVenda
    {
        AGUARDANDO_PAGAMENTO,
        PAGAMENTO_EFETUADO,
        ENVIADO_TRANSPORTADORA,
        CANCELADO,
        ENTREGUE
    }
}
namespace tech_test_payment_api.Domain.Entities
{
    public class ItemVenda
    {
        private int _itemVendaId;
        private int _vendaId;
        private string _nomeProduto;
        private decimal _precoProduto;

        public int ItemVendaId
        {
            get { return this._itemVendaId; }
            set { this._itemVendaId = value; }
        }

        public int VendaId
        {
            get { return this._vendaId; }
            set { this._vendaId = value; }
        }

        public string NomeProduto
        {
            get { return this._nomeProduto; }
            set { this._nomeProduto = value; }
        }

        public decimal PrecoProduto
        {
            get { return this._precoProduto; }
            set { this._precoProduto = value; }
        }
    }
}

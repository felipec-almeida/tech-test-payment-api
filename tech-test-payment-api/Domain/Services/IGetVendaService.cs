﻿using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Domain.Services;

public interface IGetVendaService
{
    public Task<Venda?> Executar(int id);
}

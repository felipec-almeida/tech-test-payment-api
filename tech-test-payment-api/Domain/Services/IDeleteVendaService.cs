﻿namespace tech_test_payment_api.Domain.Services
{
    public interface IDeleteVendaService
    {
        public Task<string> Executar(int id);
    }
}

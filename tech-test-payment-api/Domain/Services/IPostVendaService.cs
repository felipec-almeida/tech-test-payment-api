﻿using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Domain.Services;

public interface IPostVendaService
{
    public Task<Venda> Executar(Venda venda);
}

﻿using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Domain.Services
{
    public interface IPatchStatusVendaService
    {
        public Task<Venda> Executar(int id, EStatusVenda status);
    }
}

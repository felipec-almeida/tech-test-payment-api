﻿using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Services
{
    public class DeleteVendaService : IDeleteVendaService
    {
        private readonly IVendaRepository _vendaRepository;

        public DeleteVendaService(IVendaRepository vendaRepository)
        {
            this._vendaRepository = vendaRepository;
        }

        public async Task<string> Executar(int id)
        {
            var venda = await _vendaRepository.Get(id);

            Validar(venda);

            var result = await _vendaRepository.DeleteVenda(venda);

            return result;
        }

        private void Validar(Venda venda)
        {
            if (venda == null)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_VENDA);
        }
    }
}

﻿using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Services
{
    public class DeleteItemVendaService : IDeleteItemVendaService
    {
        private readonly IVendaRepository _vendaRepository;

        public DeleteItemVendaService(IVendaRepository vendaRepository)
        {
            this._vendaRepository = vendaRepository;
        }

        public async Task<Venda> Executar(int id, int itemId)
        {
            var venda = await _vendaRepository.Get(id);
            var item = await _vendaRepository.GetItem(itemId);

            Validar(venda, item);

            venda = await _vendaRepository.DeleteItemVenda(venda, item);

            return venda;
        }

        private void Validar(Venda venda, ItemVenda item)
        {
            if (venda == null)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_VENDA);

            if (item == null)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM);

            if (!venda.ItemVenda.Exists(x => x.ItemVendaId == item.ItemVendaId))
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM_NAO_EXISTENTE);

            if (venda.StatusVenda != EStatusVenda.AGUARDANDO_PAGAMENTO)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM_STATUS_VENDA);
        }
    }
}

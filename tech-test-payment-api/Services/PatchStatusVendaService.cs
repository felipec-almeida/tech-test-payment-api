﻿using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Services
{
    public class PatchStatusVendaService : IPatchStatusVendaService
    {
        private IVendaRepository _vendaRepository;

        public PatchStatusVendaService(IVendaRepository vendaRepository)
        {
            _vendaRepository = vendaRepository;
        }

        public async Task<Venda> Executar(int id, EStatusVenda status)
        {
            var venda = await _vendaRepository.Get(id);

            Validar(venda.StatusVenda, status);

            venda = await _vendaRepository.UpdateStatus(venda, status);

            return venda;
        }

        private void Validar(EStatusVenda oldStatus, EStatusVenda newStatus)
        {
            if (
                oldStatus is EStatusVenda.AGUARDANDO_PAGAMENTO
                && !new[] { EStatusVenda.PAGAMENTO_EFETUADO, EStatusVenda.CANCELADO }.Contains(
                    newStatus
                )
            )
                throw new ValidationException(
                    ErrorConstants.ERRO_ALTERAR_STATUS_AGUARDANDO_PAGAMENTO
                );
            else if (
                oldStatus is EStatusVenda.PAGAMENTO_EFETUADO
                && !new[] { EStatusVenda.PAGAMENTO_EFETUADO, EStatusVenda.CANCELADO }.Contains(
                    newStatus
                )
            )
                throw new ValidationException(
                    ErrorConstants.ERRO_ALTERAR_STATUS_PAGAMENTO_EFETUADO
                );
            else if (
                oldStatus is EStatusVenda.ENVIADO_TRANSPORTADORA
                && !new[] { EStatusVenda.ENTREGUE, EStatusVenda.CANCELADO }.Contains(newStatus)
            )
                throw new ValidationException(
                    ErrorConstants.ERRO_ALTERAR_STATUS_ENVIADO_TRANSPORTADORA
                );
        }
    }
}

﻿using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Services
{
    public class PostItemVendaService : IPostItemVendaService
    {
        private IVendaRepository _vendaRepository;

        public PostItemVendaService(IVendaRepository vendaRepository)
        {
            _vendaRepository = vendaRepository;
        }

        public async Task<Venda> Executar(int id, ItemVenda item)
        {
            var venda = await _vendaRepository.Get(id);

            Validar(venda, item);

            venda = await _vendaRepository.SaveItemVenda(venda, item);

            return venda;
        }

        private void Validar(Venda venda, ItemVenda item)
        {
            if (venda == null)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_VENDA);

            if (item == null)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM);

            if (venda.ItemVenda.Exists(x => x.ItemVendaId == item.ItemVendaId))
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM_JA_EXISTENTE);

            if (venda.StatusVenda != EStatusVenda.AGUARDANDO_PAGAMENTO)
                throw new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM_STATUS_VENDA);
        }
    }
}

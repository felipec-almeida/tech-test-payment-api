﻿using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Services;

public class GetVendaService : IGetVendaService
{
    private IVendaRepository _vendaRepository;

    public GetVendaService(IVendaRepository vendaRepository)
    {
        _vendaRepository = vendaRepository;
    }

    public async Task<Venda?> Executar(int id)
    {
        var venda = await _vendaRepository.Get(id);

        if (venda == null)
            throw new ValidationException(ErrorConstants.ERRO_ENVIAR_VENDA);

        return venda;
    }
}

﻿using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Services;

public class PostVendaService : IPostVendaService
{
    private readonly IVendaRepository _vendaRepository;

    public PostVendaService(IVendaRepository vendaRepository)
    {
        this._vendaRepository = vendaRepository;
    }

    public async Task<Venda> Executar(Venda venda)
    {
        Validar(venda);

        venda.StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO;

        return await this._vendaRepository.Save(venda);
    }

    private void Validar(Venda venda)
    {
        if (venda == null)
            throw new ValidationException(ErrorConstants.ERRO_ENVIAR_VENDA);

        if (!venda.ItemVenda.Any())
            throw new ValidationException(ErrorConstants.ERRO_ENVIAR_VENDA_ITEM);
    }
}

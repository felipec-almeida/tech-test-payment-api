using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Repository;
using tech_test_payment_api.Repository.Context;
using tech_test_payment_api.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<ApiContext>(
    options => options.UseInMemoryDatabase("InMemoryDataBase")
);

builder.Services
    .AddControllers()
    .AddJsonOptions(
        options => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter())
    )
    .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

builder.Services.AddControllers();

// Serviços
builder.Services.AddScoped<IGetVendaService, GetVendaService>();
builder.Services.AddScoped<IPostVendaService, PostVendaService>();
builder.Services.AddScoped<IPatchStatusVendaService, PatchStatusVendaService>();
builder.Services.AddScoped<IPostItemVendaService, PostItemVendaService>();
builder.Services.AddScoped<IDeleteItemVendaService, DeleteItemVendaService>();
builder.Services.AddScoped<IDeleteVendaService, DeleteVendaService>();

// Repositorios
builder.Services.AddScoped<IVendaRepository, VendaRepository>();

// Configuração do Swagger
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Teste Técnico Pottencial", Version = "v1" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

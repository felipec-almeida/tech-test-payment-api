﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Domain.Entities;

namespace tech_test_payment_api.Repository.Context
{
    public class ApiContext : DbContext
    {
        public virtual DbSet<ItemVenda> ItemVenda { get; set; }
        public virtual DbSet<Venda> Vendas { get; set; }
        public virtual DbSet<Vendedor> Vendedores { get; set; }

        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options) { }
    }
}

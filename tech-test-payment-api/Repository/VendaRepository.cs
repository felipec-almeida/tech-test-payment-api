﻿using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Repository.Context;

namespace tech_test_payment_api.Repository;

public class VendaRepository : IVendaRepository
{
    private readonly ApiContext _context;

    public VendaRepository(ApiContext context)
    {
        _context = context;
    }

    public async Task<Venda?> Get(int id)
    {
        var vendaResult = await _context.Vendas.FirstOrDefaultAsync(x => x.VendaId == id);

        if (vendaResult is null)
            return null;

        vendaResult.ItemVenda = await _context.ItemVenda.Where(x => x.VendaId == id).ToListAsync();

        vendaResult.Vendedor = await _context.Vendedores.FirstAsync(
            x => x.VendedorId == vendaResult.VendedorId
        );

        return vendaResult;
    }

    public async Task<ItemVenda?> GetItem(int id)
    {
        var itemResult = await _context.ItemVenda.FirstOrDefaultAsync(x => x.ItemVendaId == id);

        if (itemResult is null)
            return null;

        return itemResult;
    }

    public async Task<Venda> Save(Venda venda)
    {
        await _context.ItemVenda.AddRangeAsync(venda.ItemVenda);
        await _context.Vendedores.AddAsync(venda.Vendedor);
        await _context.Vendas.AddAsync(venda);
        await _context.SaveChangesAsync();

        return venda;
    }

    public async Task<Venda> UpdateStatus(Venda venda, EStatusVenda status)
    {
        venda.StatusVenda = status;
        _context.Vendas.Update(venda);
        await _context.SaveChangesAsync();

        return venda;
    }

    public async Task<Venda> SaveItemVenda(Venda venda, ItemVenda item)
    {
        venda.ItemVenda.Add(item);
        _context.Vendas.Update(venda);
        await _context.ItemVenda.AddAsync(item);
        await _context.SaveChangesAsync();

        return venda;
    }

    public async Task<Venda> DeleteItemVenda(Venda venda, ItemVenda item)
    {
        venda.ItemVenda.Remove(item);
        _context.Vendas.Update(venda);
        _context.ItemVenda.Remove(item);
        await _context.SaveChangesAsync();

        return venda;
    }

    public async Task<string> DeleteVenda(Venda venda)
    {
        var vendaId = venda.VendaId;

        _context.Vendas.Remove(venda);
        await _context.SaveChangesAsync();

        return $"Venda de ID {vendaId} deletada com sucesso";
    }
}

using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Controllers.Base;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;
using tech_test_payment_api.Repository.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [Authorize]
    public class VendasController : BaseController
    {
        private readonly ApiContext _context;
        private readonly IGetVendaService _getVendaService;
        private readonly IPostVendaService _postVendaService;
        private readonly IPatchStatusVendaService _patchStatusVendaService;
        private readonly IPostItemVendaService _postItemVendaService;
        private readonly IDeleteItemVendaService _deleteItemVendaService;
        private readonly IDeleteVendaService _deleteVendaService;

        public VendasController(
            ApiContext context,
            IGetVendaService getVendaService,
            IPostVendaService postVendaService,
            IPatchStatusVendaService patchStatusVendaService,
            IPostItemVendaService postItemVendaService,
            IDeleteItemVendaService deleteItemVendaService,
            IDeleteVendaService deleteVendaService
        )
        {
            _context = context;
            _getVendaService = getVendaService;
            _postVendaService = postVendaService;
            _patchStatusVendaService = patchStatusVendaService;
            _postItemVendaService = postItemVendaService;
            _deleteItemVendaService = deleteItemVendaService;
            _deleteVendaService = deleteVendaService;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Venda))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(
            StatusCodes.Status500InternalServerError,
            Type = typeof(BaseResponseError)
        )]
        public async Task<IActionResult> GetVenda([FromRoute] [Required] int id)
        {
            return await ResponseAsync<Venda>(_getVendaService.Executar(id));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Venda))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BaseResponseError))]
        public async Task<IActionResult> PostVenda([FromBody] [Required] Venda venda)
        {
            return await ResponseAsync<Venda>(_postVendaService.Executar(venda));

            #region ToDo: Rota de Autenticação da API

            //var validationToken = this._tokenService.ValidateToken();
            //var token = string.Empty;

            //if (string.IsNullOrEmpty(validationToken))
            //{
            //    token = this._tokenService.GenerateToken(venda.Vendedor);
            //    return Ok(new BaseResponseToken<Venda>(venda, token));
            //}
            //else
            //{
            //    if (!validationToken.Equals(venda.Vendedor.Email))
            //    {
            //        token = this._tokenService.GenerateToken(venda.Vendedor);
            //        return Ok(new BaseResponseToken<Venda>(venda, token));
            //    }
            //    else
            //        return Ok(venda);
            //}

            #endregion ToDo: Rota de Autenticação da API
        }

        [HttpPatch("{id}/atualizarStatus")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Venda))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BaseResponseError))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BaseResponseError))]
        [ProducesResponseType(
            StatusCodes.Status500InternalServerError,
            Type = typeof(BaseResponseError)
        )]
        public async Task<IActionResult> PatchStatusVenda(
            [FromRoute] [Required] int id,
            [FromQuery] [Required] EStatusVenda status
        )
        {
            return await ResponseAsync<Venda>(_patchStatusVendaService.Executar(id, status));
        }

        [HttpPost("{id}/inserirItem")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Venda))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BaseResponseError))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BaseResponseError))]
        public async Task<IActionResult> PatchItemVenda(
            [FromRoute] [Required] int id,
            [FromBody] [Required] ItemVenda item
        )
        {
            return await ResponseAsync<Venda>(_postItemVendaService.Executar(id, item));
        }

        [HttpDelete("{id}/removerItem/{itemId}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Venda))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(BaseResponseError))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(BaseResponseError))]
        public async Task<IActionResult> RemoverItemVenda(
            [FromRoute] [Required] int id,
            [FromRoute] [Required] int itemId
        )
        {
            return await ResponseAsync<Venda>(_deleteItemVendaService.Executar(id, itemId));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoverVenda([FromRoute] [Required] int id)
        {
            return await ResponseAsync<string>(_deleteVendaService.Executar(id));
        }
    }
}

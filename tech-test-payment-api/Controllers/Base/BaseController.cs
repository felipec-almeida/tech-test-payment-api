﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Shared;

namespace tech_test_payment_api.Controllers.Base;

public class BaseController : Controller
{
    public async Task<IActionResult> ResponseAsync<T>(Task<T> task)
    {
        try
        {
            var response = await task;

            return Ok(response);
        }
        catch (NotFoundException)
        {
            return NotFound();
        }
        catch (ValidationException validationException)
        {
            return BadRequest(new BaseResponseError(validationException.Message));
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}

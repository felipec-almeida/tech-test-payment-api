﻿using FluentAssertions;
using Moq;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;
using tech_test_payment_api.Services;

namespace tech_test_payment_api_tests
{
    public class DeleteVendaTest
    {
        private readonly Mock<IVendaRepository> _vendaRepository;
        private readonly IDeleteVendaService _deleteVendaService;

        public DeleteVendaTest()
        {
            _vendaRepository = new Mock<IVendaRepository>();
            _deleteVendaService = new DeleteVendaService(_vendaRepository.Object);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_VendaNaoEncontrada_DarErro(int id)
        {
            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync((Venda)null);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _deleteVendaService.Executar(id)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_VENDA);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_ExcluirVendaCorretamente(int id)
        {
            var venda = new Venda { VendaId = id, StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository
                .Setup(x => x.DeleteVenda(venda))
                .ReturnsAsync("Venda excluída com sucesso.");

            var result = await _deleteVendaService.Executar(id);

            result.Should().Be("Venda excluída com sucesso.");
        }
    }
}

﻿using FluentAssertions;
using Moq;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;
using tech_test_payment_api.Services;

namespace tech_test_payment_api_tests
{
    public class PatchStatusVendaTest
    {
        private readonly Mock<IVendaRepository> _vendaRepository;
        private readonly IPatchStatusVendaService _patchStatusVendaService;

        public PatchStatusVendaTest()
        {
            _vendaRepository = new Mock<IVendaRepository>();
            _patchStatusVendaService = new PatchStatusVendaService(_vendaRepository.Object);
        }

        [Theory]
        [InlineData(1, EStatusVenda.AGUARDANDO_PAGAMENTO, EStatusVenda.PAGAMENTO_EFETUADO)]
        [InlineData(1, EStatusVenda.AGUARDANDO_PAGAMENTO, EStatusVenda.CANCELADO)]
        [InlineData(1, EStatusVenda.PAGAMENTO_EFETUADO, EStatusVenda.PAGAMENTO_EFETUADO)]
        [InlineData(1, EStatusVenda.PAGAMENTO_EFETUADO, EStatusVenda.CANCELADO)]
        [InlineData(1, EStatusVenda.ENVIADO_TRANSPORTADORA, EStatusVenda.ENTREGUE)]
        [InlineData(1, EStatusVenda.ENVIADO_TRANSPORTADORA, EStatusVenda.CANCELADO)]
        public async Task Executar_AtualizarStatusCorretamente(
            int id,
            EStatusVenda initialStatus,
            EStatusVenda newStatus
        )
        {
            var venda = new Venda { VendaId = id, StatusVenda = initialStatus };

            _vendaRepository.Setup(r => r.Get(id)).ReturnsAsync(venda);
            _vendaRepository
                .Setup(r => r.UpdateStatus(It.IsAny<Venda>(), newStatus))
                .ReturnsAsync(new Venda { VendaId = id, StatusVenda = newStatus });

            var result = await _patchStatusVendaService.Executar(id, newStatus);
            result.VendaId.Should().Be(id);
            result.StatusVenda.Should().Be(newStatus);
        }

        [Theory]
        [InlineData(1, EStatusVenda.AGUARDANDO_PAGAMENTO, EStatusVenda.ENVIADO_TRANSPORTADORA)]
        [InlineData(1, EStatusVenda.AGUARDANDO_PAGAMENTO, EStatusVenda.ENTREGUE)]
        public async Task Executar_StautsVendaAguardandoPagamentoDiferente_DarErro(
            int id,
            EStatusVenda initialStatus,
            EStatusVenda newStatus
        )
        {
            var venda = new Venda { VendaId = id, StatusVenda = initialStatus };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository
                .Setup(x => x.UpdateStatus(venda, newStatus))
                .ThrowsAsync(
                    new ValidationException(ErrorConstants.ERRO_ALTERAR_STATUS_AGUARDANDO_PAGAMENTO)
                );

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _patchStatusVendaService.Executar(id, newStatus)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ALTERAR_STATUS_AGUARDANDO_PAGAMENTO);
        }

        [Theory]
        [InlineData(1, EStatusVenda.PAGAMENTO_EFETUADO, EStatusVenda.ENVIADO_TRANSPORTADORA)]
        [InlineData(1, EStatusVenda.PAGAMENTO_EFETUADO, EStatusVenda.ENTREGUE)]
        public async Task Executar_StatusVendaPagamentoEfetuadoDiferente_DarErro(
            int id,
            EStatusVenda initialStatus,
            EStatusVenda newStatus
        )
        {
            var venda = new Venda { VendaId = id, StatusVenda = initialStatus };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository
                .Setup(x => x.UpdateStatus(venda, newStatus))
                .ThrowsAsync(
                    new ValidationException(ErrorConstants.ERRO_ALTERAR_STATUS_PAGAMENTO_EFETUADO)
                );

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _patchStatusVendaService.Executar(id, newStatus)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ALTERAR_STATUS_PAGAMENTO_EFETUADO);
        }

        [Theory]
        [InlineData(1, EStatusVenda.ENVIADO_TRANSPORTADORA, EStatusVenda.AGUARDANDO_PAGAMENTO)]
        [InlineData(1, EStatusVenda.ENVIADO_TRANSPORTADORA, EStatusVenda.PAGAMENTO_EFETUADO)]
        public async Task Executar_StatusVendaEnviadoTransportadoraDiferente_DarErro(
            int id,
            EStatusVenda initialStatus,
            EStatusVenda newStatus
        )
        {
            var venda = new Venda { VendaId = id, StatusVenda = initialStatus };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository
                .Setup(x => x.UpdateStatus(venda, newStatus))
                .ThrowsAsync(
                    new ValidationException(
                        ErrorConstants.ERRO_ALTERAR_STATUS_ENVIADO_TRANSPORTADORA
                    )
                );

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _patchStatusVendaService.Executar(id, newStatus)
            );

            exception.Message
                .Should()
                .Be(ErrorConstants.ERRO_ALTERAR_STATUS_ENVIADO_TRANSPORTADORA);
        }
    }
}

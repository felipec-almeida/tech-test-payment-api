﻿using FluentAssertions;
using Moq;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;
using tech_test_payment_api.Services;

namespace tech_test_payment_api_tests
{
    public class PostItemVendaTest
    {
        private readonly Mock<IVendaRepository> _vendaRepository;
        private readonly IPostItemVendaService _postItemVendaService;

        public PostItemVendaTest()
        {
            _vendaRepository = new Mock<IVendaRepository>();
            _postItemVendaService = new PostItemVendaService(_vendaRepository.Object);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_VendaNaoEncontrada_DarErro(int id)
        {
            ItemVenda item = new ItemVenda
            {
                ItemVendaId = 1,
                NomeProduto = "Caneta",
                PrecoProduto = 10.0M
            };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync((Venda)null);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _postItemVendaService.Executar(id, item)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_VENDA);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_ItemNulo_DarErro(int id)
        {
            var venda = new Venda { VendaId = id, StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO };
            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _postItemVendaService.Executar(id, null)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_ITEM);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_ItemJaExistente_DarErro(int id)
        {
            var existingItem = new ItemVenda
            {
                ItemVendaId = 1,
                NomeProduto = "Caneta",
                PrecoProduto = 10.0M
            };
            var venda = new Venda
            {
                VendaId = id,
                StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO,
                ItemVenda = new List<ItemVenda> { existingItem }
            };

            var newItem = new ItemVenda
            {
                ItemVendaId = 1,
                NomeProduto = "Lápis",
                PrecoProduto = 5.0M
            };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _postItemVendaService.Executar(id, newItem)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_ITEM_JA_EXISTENTE);
        }

        [Theory]
        [InlineData(1, EStatusVenda.PAGAMENTO_EFETUADO)]
        [InlineData(1, EStatusVenda.CANCELADO)]
        [InlineData(1, EStatusVenda.ENVIADO_TRANSPORTADORA)]
        [InlineData(1, EStatusVenda.ENTREGUE)]
        public async Task Executar_StatusVendaIncorreto_DarErro(int id, EStatusVenda status)
        {
            var venda = new Venda { VendaId = id, StatusVenda = status };

            var item = new ItemVenda
            {
                ItemVendaId = 2,
                NomeProduto = "Caneta",
                PrecoProduto = 10.0M
            };

            _vendaRepository
                .Setup(x => x.Get(id))
                .ThrowsAsync(new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM_STATUS_VENDA));

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _postItemVendaService.Executar(id, item)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_ITEM_STATUS_VENDA);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_AdicionarItemCorretamente(int id)
        {
            ItemVenda itemVenda = new ItemVenda
            {
                ItemVendaId = 1,
                NomeProduto = "Caneta",
                PrecoProduto = 10.0M
            };

            var venda = new Venda
            {
                VendaId = id,
                StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO,
                ItemVenda = new List<ItemVenda>() { itemVenda }
            };

            var novoItem = new ItemVenda
            {
                ItemVendaId = 2,
                NomeProduto = "Lápis",
                PrecoProduto = 5.0M
            };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);

            _vendaRepository
                .Setup(x => x.SaveItemVenda(venda, novoItem))
                .ReturnsAsync(
                    new Venda
                    {
                        VendaId = id,
                        StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO,
                        ItemVenda = new List<ItemVenda> { itemVenda, novoItem }
                    }
                );

            var result = await _postItemVendaService.Executar(id, novoItem);

            result.ItemVenda.Should().ContainSingle(i => i.ItemVendaId == novoItem.ItemVendaId);
        }
    }
}

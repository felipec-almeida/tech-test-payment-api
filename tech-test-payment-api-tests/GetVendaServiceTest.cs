﻿using FluentAssertions;
using Moq;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Services;

namespace tech_test_payment_api_tests
{
    public class GetVendaServiceTest
    {
        private readonly IGetVendaService _getVendaService;
        private readonly Mock<IVendaRepository> _vendaRepository;

        public GetVendaServiceTest()
        {
            _vendaRepository = new Mock<IVendaRepository>();
            _getVendaService = new GetVendaService(_vendaRepository.Object);
        }

        [Fact]
        public async Task Executar_BuscarId_NaoEncontrado()
        {
            Venda venda = null;
            _vendaRepository
                .Setup(x => x.Get(It.IsAny<int>()))
                .ThrowsAsync(new NotFoundException());

            var exception = Assert.ThrowsAsync<NotFoundException>(
                () => _getVendaService.Executar(1)
            );

            exception.Result.Message.Should().NotBeNull();
        }

        [Theory]
        [InlineData(2, EStatusVenda.ENTREGUE)]
        [InlineData(3, EStatusVenda.PAGAMENTO_EFETUADO)]
        public async Task Executar_BuscarId_Encontrada(int id, EStatusVenda status)
        {
            var venda = new Venda() { VendaId = id, StatusVenda = status };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);

            var result = await _getVendaService.Executar(id);
            result.VendaId.Should().Be(id);
            result.StatusVenda.Should().Be(status);
        }
    }
}

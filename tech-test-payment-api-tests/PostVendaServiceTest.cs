﻿using FluentAssertions;
using Moq;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;
using tech_test_payment_api.Services;

namespace tech_test_payment_api_tests
{
    public class PostVendaServiceTest
    {
        private readonly Mock<IVendaRepository> _vendaRepository;
        private readonly IPostVendaService _postVendaService;

        public PostVendaServiceTest()
        {
            _vendaRepository = new Mock<IVendaRepository>();
            _postVendaService = new PostVendaService(_vendaRepository.Object);
        }

        [Fact]
        public async Task Executar_QuandoEnviarNulo_DarErro()
        {
            _vendaRepository.Setup(x => x.Get(It.IsAny<int>()));

            var exception = Assert.ThrowsAsync<ValidationException>(
                () => _postVendaService.Executar(null)
            );

            exception.Result.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_VENDA);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public async Task Executar_GravarCorretamente(int id)
        {
            var venda = new Venda()
            {
                VendaId = id,
                Data = DateTime.Now,
                VendedorId = 1,
                Vendedor = new Vendedor()
                {
                    VendedorId = 1,
                    Email = "exemplo@email.com",
                    CPF = "12345678901",
                    NomeCompleto = "Exemplo Nome Completo",
                    Telefone = "978443356"
                },
                ItemVenda = new List<ItemVenda>()
                {
                    new ItemVenda()
                    {
                        ItemVendaId = 1,
                        NomeProduto = "Caneta Preta",
                        PrecoProduto = 10.1M,
                        VendaId = id
                    }
                },
                StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO
            };

            _vendaRepository.Setup(x => x.Save(It.IsAny<Venda>())).ReturnsAsync(venda);

            var result = await _postVendaService.Executar(venda);

            result.Should().NotBeNull();
            result.VendaId.Should().Be(id);
            result.Vendedor.Should().NotBeNull();
            result.ItemVenda.Should().NotBeEmpty();
            result.StatusVenda.Should().Be(EStatusVenda.AGUARDANDO_PAGAMENTO);
        }

        [Theory]
        [InlineData(1, new object[] { })]
        public async Task Executar_QuandoEnviarNenhumItem_DarErro(int id, object itens)
        {
            var venda = new Venda()
            {
                VendaId = id,
                Data = DateTime.Now,
                VendedorId = 1,
                Vendedor = new Vendedor()
                {
                    VendedorId = 1,
                    Email = "exemplo@email.com",
                    CPF = "12345678901",
                    NomeCompleto = "Exemplo Nome Completo",
                    Telefone = "978443356"
                },
                ItemVenda = itens is null ? null : new List<ItemVenda>(),
                StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO
            };

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _postVendaService.Executar(venda)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_VENDA_ITEM);
        }
    }
}

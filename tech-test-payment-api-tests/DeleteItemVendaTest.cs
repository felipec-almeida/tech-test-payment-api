﻿using FluentAssertions;
using Moq;
using tech_test_payment_api.Domain.Entities;
using tech_test_payment_api.Domain.Exceptions;
using tech_test_payment_api.Domain.Repository;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Domain.Shared;
using tech_test_payment_api.Services;

namespace tech_test_payment_api_tests
{
    public class DeleteItemVendaServiceTest
    {
        private readonly Mock<IVendaRepository> _vendaRepository;
        private readonly IDeleteItemVendaService _deleteItemVendaService;

        public DeleteItemVendaServiceTest()
        {
            _vendaRepository = new Mock<IVendaRepository>();
            _deleteItemVendaService = new DeleteItemVendaService(_vendaRepository.Object);
        }

        [Theory]
        [InlineData(1, 1)]
        public async Task Executar_VendaNaoEncontrada_DarErro(int id, int itemId)
        {
            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync((Venda)null);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _deleteItemVendaService.Executar(id, itemId)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_VENDA);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_ItemNulo_DarErro(int id)
        {
            var venda = new Venda { VendaId = id, StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO };
            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository.Setup(x => x.GetItem(It.IsAny<int>())).ReturnsAsync((ItemVenda)null);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _deleteItemVendaService.Executar(id, 1)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_ITEM);
        }

        [Theory]
        [InlineData(1, 2)]
        public async Task Executar_ItemNaoExistente_DarErro(int id, int itemIdDelete)
        {
            var itemExistente = new ItemVenda
            {
                ItemVendaId = 1,
                NomeProduto = "Caneta",
                PrecoProduto = 10.0M
            };

            var venda = new Venda
            {
                VendaId = id,
                StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO,
                ItemVenda = new List<ItemVenda> { itemExistente }
            };

            var itemVendaDelete = new ItemVenda { ItemVendaId = itemIdDelete };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository.Setup(x => x.GetItem(itemIdDelete)).ReturnsAsync(itemVendaDelete);

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _deleteItemVendaService.Executar(id, itemIdDelete)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_ITEM_NAO_EXISTENTE);
        }

        [Theory]
        [InlineData(1, EStatusVenda.PAGAMENTO_EFETUADO)]
        [InlineData(1, EStatusVenda.CANCELADO)]
        [InlineData(1, EStatusVenda.ENVIADO_TRANSPORTADORA)]
        [InlineData(1, EStatusVenda.ENTREGUE)]
        public async Task Executar_StatusVendaIncorreto_DarErro(int id, EStatusVenda status)
        {
            var venda = new Venda { VendaId = id, StatusVenda = status };
            var itemIdDelete = 1;

            _vendaRepository
                .Setup(x => x.Get(id))
                .ThrowsAsync(new ValidationException(ErrorConstants.ERRO_ENVIAR_ITEM_STATUS_VENDA));

            _vendaRepository
                .Setup(x => x.GetItem(itemIdDelete))
                .ReturnsAsync(new ItemVenda { ItemVendaId = itemIdDelete });

            var exception = await Assert.ThrowsAsync<ValidationException>(
                () => _deleteItemVendaService.Executar(id, itemIdDelete)
            );

            exception.Message.Should().Be(ErrorConstants.ERRO_ENVIAR_ITEM_STATUS_VENDA);
        }

        [Theory]
        [InlineData(1)]
        public async Task Executar_ExcluirItemCorretamente(int id)
        {
            var itemDelete = new ItemVenda
            {
                ItemVendaId = 1,
                NomeProduto = "Caneta",
                PrecoProduto = 10.0M
            };

            var venda = new Venda
            {
                VendaId = id,
                StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO,
                ItemVenda = new List<ItemVenda> { itemDelete }
            };

            _vendaRepository.Setup(x => x.Get(id)).ReturnsAsync(venda);
            _vendaRepository.Setup(x => x.GetItem(itemDelete.ItemVendaId)).ReturnsAsync(itemDelete);

            _vendaRepository
                .Setup(x => x.DeleteItemVenda(venda, itemDelete))
                .ReturnsAsync(
                    new Venda
                    {
                        VendaId = id,
                        StatusVenda = EStatusVenda.AGUARDANDO_PAGAMENTO,
                        ItemVenda = new List<ItemVenda>() // A lista deve estar vazia após a exclusão
                    }
                );

            var result = await _deleteItemVendaService.Executar(id, itemDelete.ItemVendaId);

            result.ItemVenda.Should().BeEmpty();
        }
    }
}
